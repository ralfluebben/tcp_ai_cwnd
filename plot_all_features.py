feature_columns = ["ts",
"saddr",
"sport",
"daddr",
"dport",
"sock_cookie",
"tcpi_state", 
"tcpi_pacing_rate", 
"tcpi_max_pacing_rate",
"tcpi_snd_cwnd", 
"tcpi_min_rtt", 
"tcpi_ca_state", 
"tcpi_retransmits", 
"tcpi_probes",
"tcpi_backoff",
"tcpi_rto",
"tcpi_ato",
"tcpi_last_data_sent",
"tcpi_last_data_recv",
"tcpi_last_ack_recv",
"tcpi_pmtu",
"tcpi_rcv_ssthresh",
"tcpi_rtt",
"tcpi_rttvar",
"tcpi_snd_ssthresh",
"tcpi_advmss",
"tcpi_rcv_rtt",
"tcpi_rcv_space",
"tcpi_total_retrans",
"tcpi_bytes_acked",
"tcpi_bytes_received",
"tcpi_notsent_bytes",
"tcpi_segs_out",
"tcpi_segs_in",
"tcpi_busy_time",
"tcpi_rwnd_limited",
"tcpi_sndbuf_limited",
"tcpi_data_segs_in",
"tcpi_data_segs_out",
"tcpi_delivery_rate_app_limited",
"tcpi_delivery_rate",
"tcpi_delivered",
"tcpi_delivered_ce",
"tcpi_bytes_sent",
"tcpi_bytes_retrans",
"tcpi_dsack_dups",
"tcpi_reord_seen",
"tcpi_rcv_ooopack",
"tcpi_fastopen_client_fail",
"bbr_bw",
"bbr_minrtt",
"bbr_pacing_gain",
"bbr_cwnd_gain", ]

import numpy as np
import pandas as pd
import os
from multiprocessing import Pool
import sys
import gc
import feather
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

c=1
fpath="tcp_" + str(c) + ".trace" 
data1=pd.read_csv(fpath, sep=' ', usecols=feature_columns)
print(data1.columns)
# split by cookie
d = None
min_bytes_acked = 100000
min_bw = 1000
train_len = 300
f = feature_columns.index("sock_cookie")
cookies = np.unique(data1["sock_cookie"].to_numpy())
i_ack = feature_columns.index("tcpi_bytes_acked")
i_bw = feature_columns.index("bbr_bw")
i_ts = feature_columns.index("ts")
data_list = []
for f in feature_columns:
    idx = feature_columns.index(f)
    max_n=25
    plt.figure(figsize=(12, int(3*max_n)))
    for n in range(max_n):
        cookie=cookies[n]
        data = data1[(data1["sock_cookie"]==cookie)].to_numpy()
        ax=plt.subplot(max_n, 1, n+1)
        # shift ts to zero
        bw=np.nan_to_num(data[0:10,i_bw].astype(np.float))
        if (data[-1,i_ack] > min_bytes_acked) and (np.mean(bw) > 1000) and (data.shape[0] >= train_len):
            plt.plot(data[0:train_len,idx], label=f, marker='.', zorder=-10)
        else:
            print("Skip plot")
    plt.xlabel('packet [#]')
    plt.tight_layout()
    fn="feature_plots/"+f+".pdf"
    print(fn)
    plt.savefig(fn)
    plt.close()