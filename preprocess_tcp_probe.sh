echo "time snd_nxt snd_una snd_cwnd ssthresh snd_wnd srtt rcv_wnd cookie min_rtt rtt_var delivery_rate"  > tcp.trace
nice -n 19 zcat server_trace.txt.gz | nice -n 19 grep "10\]:6" | nice -n 19 awk '/tcp_probe:/{split($0,a,"tcp_probe:"); n=split(a[1],b," "); gsub(":","",b[n]); gsub(/^[ \t]+/,"",b[n]); gsub("="," ",a[2]); split(a[2],c," "); printf "%s", b[n]; for (i in c) { printf " %s",c[i]}; print"" }'  | nice -n 19 cut -f 1,7,8,9,10,11,12,13,14,15,16,17 -d ' ' >> tcp.trace
python3 ~/git/tcp_ai_cwnd/preprocess_tcp_probe.py
rm tcp.trace
