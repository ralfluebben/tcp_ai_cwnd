# Server
```
sudo apt install nginx
dd if=/dev/urandom of=data.bin bs=838860 count=1
sudo mv data.bin /var/www/html/
```

# all
```
declare -a arr=("server" "client" "enb0" "gateway")
for s in "${arr[@]}"
do
	echo $s;
    vagrant ssh ${s} -c  "sudo apt update; sudo apt install -y ethtool conntrack bc"
done

for s in "${arr[@]}"
do
	echo $s
	IP=`vagrant ssh ${s} -c "ip addr show eth0 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`
	sshpass -p vagrant ssh-copy-id -o "UserKnownHostsFile=/dev/null" -o StrictHostKeyChecking=no vagrant@${IP}
    #vagrant ssh ${s} -c "sudo reboot"
done

```

# client
```
wget https://apache.mirror.iphh.net//jmeter/binaries/apache-jmeter-5.4.1.tgz
tar xvzf apache-jmeter-5.4.1.tgz
```

# server
declare -a arr=("server")
for s in "${arr[@]}";
do
	echo $s
	vagrant ssh ${s} -c 'export DEBIAN_FRONTEND=noninteractive; sudo apt-get update; sudo -E apt-get -y -o "Dpkg::Options::=--force-confdef" -o "Dpkg::Options::=--force-confold" upgrade'
	vagrant ssh ${s} -c "sudo apt -y install tcpdump iperf3 trace-cmd libmnl-dev pkg-config"
	vagrant ssh ${s} -c "sudo apt -y install make gcc; wget https://mirrors.edge.kernel.org/pub/software/network/ethtool/ethtool-5.13.tar.gz; tar xvzf ethtool-5.13.tar.gz; cd ethtool-5.13/; ./configure; make; sudo make install; cd ..; rm -rf ethtool-5.13*"
done

# server ubuntu hirsuite
- cc bpg requires libc > 2.32

- on host
  - zstd clang llvm
  - bpf must be build in
  - clang/llvm 13 is required
```
sudo vi /etc/update-manager/release-upgrade change to normal
sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade
do-release-upgrade -f DistUpgradeViewNonInteractive

```

# Type mismatch in recent LLVM version, rc1 of 13 is to new , LLVM 12 to old
See https://www.spinics.net/lists/bpf/msg40487.html 
use commit of 993f38d0a7942d66d2dc1a64a1702bf9b15da87a of https://github.com/llvm/llvm-project.git

```
git clone https://github.com/llvm/llvm-project.git
git checkout 993f38d0a7942d66d2dc1a64a1702bf9b15da87a
make -j`nproc`
```

- first make may fail as parallel build, run again `make`
- doc seems not to build, run again, is required for install `make ocaml_doc`
## install clang/llvm
```
sudo make install
```


# build kernel
use https://wiki.ubuntu.com/Kernel/BuildYourOwnKernel to setup the toolchain
```
sudo apt install zstd crda iw linux-cloud-tools-common wireless-regdb libfuse-dev  libmnl-dev libpopt-dev libhugetlbfs-dev
sudo git clone  <the kernel repo> 
```

Edit configs and add bbr as build in module and set as default, since modules are changes and checked by build use `skipmodule=true` for the build
https://wiki.ubuntu.com/KernelTeam/KernelMaintenance#Overriding_module_check_failures
```
LANG=C fakeroot debian/rules binary-headers binary-generic binary-perarch skipmodule=true
```

# Install the generated kernel
```
sudo dpkg -i ../*.deb
# is this required? 
~/git/linux_bbr_bpf$ cp ./debian/build/tools-perarch/include/generated/autoconf.h include/generated/
```

# Rebuild kernel
Sometimes the build fails to dirty build repo, use
```
make ARCH=x86 mrproper
git resert --hard 56778849c1ae2dddcb8a00829332d78e635feccc
LANG=C fakeroot debian/rules clean
LANG=C fakeroot debian/rules binary-headers binary-generic binary-perarch skipmodule=true
```

# bpf selftest
```
make all
```

# compile bpf programm
```
# generate the header file
bpftool btf dump file /sys/kernel/btf/vmlinux format c > vmlinux.h

# build the bpf programm
clang  -g -D__TARGET_ARCH_x86 -mlittle-endian -I/home/ubuntu/git/linux_bbr_bpf/tools/testing/selftests/bpf/tools/include -I/home/ubuntu/git/linux_bbr_bpf/tools/testing/selftests/bpf -I/home/ubuntu/git/linux_bbr_bpf/tools/include/uapi -I/home/ubuntu/git/linux_bbr_bpf/tools/testing/selftests/usr/include -idirafter /usr/local/lib/clang/13.0.0/include -idirafter /usr/local/include -idirafter /usr/include/x86_64-linux-gnu -idirafter /usr/include  -Wno-compare-distinct-pointer-types -DENABLE_ATOMICS_TESTS -O2 -target bpf -c progs/bpf_bbr.c -o /home/ubuntu/git/linux_bbr_bpf/tools/testing/selftests/bpf/bpf_bbr.o -mcpu=v3

# build the userspace program
gcc -g -rdynamic -Wall -O2 -DHAVE_GENHDR  -I./ -I../../../../tools/lib -I../../../../tools/include ./prog_tests/print_tcp_bpf.c ./tools/build/libbpf/libbpf.a -lelf -lz -o print_tcp_bpf
```
# Register and unregister the bpf programm
Register:
```
sudo bpftool struct_ops register ./bpf_bbr.o
sudo sysctl -w net.ipv4.tcp_congestion_control=bpf_bbr
```
Unregister:
```
id=$(sudo bpftool struct_ops show | cut -f 1 -d ':')
sudo bpftool struct_ops unregister id ${id}
```
# Filter output
```
zcat server_trace.txt.gz > log

echo "ts saddr sport daddr dport cookie ti_tcpi_ca_state ti_tcpi_pacing_rate ti_tcpi_snd_cwnd ti_tcpi_min_rtt ti_tcpi_ca_state ti_tcpi_retransmits ti_tcpi_probes ti_tcpi_backoff ti_tcpi_rto ti_tcpi_ato ti_tcpi_last_data_sent ti_tcpi_last_data_recv ti_tcpi_last_ack_recv ti_tcpi_pmtu ti_tcpi_rcv_ssthresh ti_tcpi_rtt ti_tcpi_rttvar ti_tcpi_snd_ssthresh ti_tcpi_advmss ti_tcpi_rcv_rtt ti_tcpi_rcv_space ti_tcpi_total_retrans ti_tcpi_bytes_acked ti_tcpi_bytes_received ti_tcpi_notsent_bytes ti_tcpi_segs_out ti_tcpi_segs_in ti_tcpi_busy_time ti_tcpi_rwnd_limited ti_tcpi_sndbuf_limited ti_tcpi_data_segs_in ti_tcpi_data_segs_out ti_tcpi_delivery_rate_app_limited ti_tcpi_delivery_rate ti_tcpi_delivered ti_tcpi_delivered_ce ti_tcpi_bytes_sent ti_tcpi_bytes_retrans ti_tcpi_dsack_dups ti_tcpi_reord_seen ti_tcpi_rcv_ooopack ti_tcpi_fastopen_client_fail bbr_bw bbr_bbr_min_rtt bbr_bbr_pacing_gain bbr_bbr_cwnd_gain" > log.1

awk '$3 ~ /80/' log >> log.1
```

s=server
IP=`vagrant ssh ${s} -c "ip addr show eth0 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`
scp ~/kernel_5.13/aug_24_15_54/*deb vagrant@${IP}:~/