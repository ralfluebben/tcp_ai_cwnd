#!/bin/bash

declare -a CONGESTIION_CONTROLS=("bbr")
declare -a CROSS_TRAFFIC=("exp")

for CONG in "${CONGESTIION_CONTROLS[@]}"
do

for CT in "${CROSS_TRAFFIC[@]}"
do

for r in `seq 1 30`
do
echo "########################################"
echo "           RUN $r                       "
echo "########################################"

set +e
vagrant halt
#set -e
vagrant up
vagrant provision

SERVER_IP=`vagrant ssh server -c "ip addr show eth0 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`
CLIENT_IP=`vagrant ssh client -c "ip addr show eth0 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`
#CLIENT1_IP=`vagrant ssh client1 -c "ip addr show eth0 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`
#CT_SERVER_IP=`vagrant ssh ct-server -c "ip addr show eth0 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`
#CT_CLIENT_IP=`vagrant ssh ct-client -c "ip addr show eth0 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`
#EXP_CLIENT_IP=`vagrant ssh client -c "ip addr show eth1 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`
#EXP_SERVER_IP=`vagrant ssh server -c "ip addr show eth1 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`
#EXP_CT_CLIENT_IP=`vagrant ssh ct-client -c "ip addr show eth1 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`
#EXP_CT_SERVER_IP=`vagrant ssh ct-server -c "ip addr show eth1 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`
#R1_IP=`vagrant ssh router1 -c "ip addr show eth0 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`
#R2_IP=`vagrant ssh router2 -c "ip addr show eth0 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`

declare -a arr_ips=("$SERVER_IP" "$CLIENT_IP" )

SL_SERVER_IP=$SERVER_IP
SL_CLIENT_IP=$CLIENT_IP


#SL_CLIENT1_IP=$CLIENT1_IP
#SL_CT_SERVER_IP=$CT_SERVER_IP
#SL_CT_CLIENT_IP=$CT_CLIENT_IP
#SL_EXP_CLIENT_IP=$EXP_CLIENT_IP
#SL_EXP_SERVER_IP=$EXP_SERVER_IP
#SL_EXP_CT_CLIENT_IP=$EXP_CT_CLIENT_IP
#SL_EXP_CT_SERVER_IP=$EXP_CT_SERVER_IP
#SL_R1_IP=$R1_IP
#SL_R2_IP=$R2_IP

export SL_SERVER_IP
export SL_CLIENT_IP
#export SL_CLIENT1_IP
#export SL_EXP_CLIENT_IP
#export SL_EXP_SERVER_IP


declare -a arr=("server" "client" "gateway" "enb0" "client")
for s in "${arr[@]}"
do
  echo "Clean up /var/log/kern.log on $s"
  vagrant ssh ${s} -c "echo > /dev/null | sudo tee /var/log/kern.log"
  vagrant ssh ${s} -c "sudo systemctl stop systemd-journald systemd-journald-dev-log.socket systemd-journald-audit.socket systemd-journald.socket"
done

for s in "${arr_ips[@]}"
do
  echo "Set congestion control on $s"
  #if [ "${CONG}" == "bbr2" ]; then
  #  if [ "$s" == "${SERVER_IP}" ]; then
  #    echo "Load TCP BBR2 module with debug $s now"
  #    ssh vagrant@${s} "sudo modprobe tcp_bbr2 bw_probe_pif_gain=511"
  #  fi
  #fi
  ssh vagrant@${s} "sudo sysctl -w net.ipv4.tcp_congestion_control=${CONG}"
  RES=`ssh vagrant@${s} "sudo sysctl net.ipv4.tcp_congestion_control | grep \"${CONG}\""`
  if [ -z "$RES" ]; then
    echo "Congestion control setting failed."
    exit -1
  fi
  tcp_slow_start_after_idle=1
  ssh vagrant@${s} "sudo sysctl -w net.ipv4.tcp_slow_start_after_idle=1"
  RES=`ssh vagrant@${s} "sudo sysctl net.ipv4.tcp_slow_start_after_idle | grep \"${tcp_slow_start_after_idle}\""`
  if [ -z "$RES" ]; then
    echo "net.ipv4.tcp_slow_start_after_idle setting failed."
    exit -1
  fi
  ssh vagrant@${s} "sudo tc qdisc add dev eth1 root fq"
  RES=`ssh vagrant@${s} "sudo tc qdisc show dev eth1 | grep \"qdisc fq\""`
  if [ -z "$RES" ]; then
    echo "qdisc setting failed."
    exit -1
  fi
  tcp_no_metrics_save=1
  ssh vagrant@${s} "sudo sysctl -w net.ipv4.tcp_no_metrics_save=1"
  RES=`ssh vagrant@${s} "sudo sysctl net.ipv4.tcp_no_metrics_save | grep \"${tcp_no_metrics_save}\""`
  if [ -z "$RES" ]; then
    echo "net.ipv4.tcp_no_metrics_save setting failed."
    exit -1
  fi
  ssh vagrant@${s} "sudo sh -c 'echo 0 > /sys/module/tcp_cubic/parameters/hystart'"
  RES=`ssh vagrant@${s} "cat /sys/module/tcp_cubic/parameters/hystart | grep '0'"`
  if [ -z "$RES" ]; then
    echo "/sys/module/tcp_cubic/parameters/hystart failed."
    exit -1
  fi
done

# ssh vagrant@${SERVER_IP} "sudo sysctl -w net.ipv4.tcp_mem='$g1 $g2 $g3'"
# RES=`ssh vagrant@${SERVER_IP} "sudo sysctl net.ipv4.tcp_mem | grep \"$g1[[:space:]]$g2[[:space:]]$g3\""`
# if [ -z "$RES" ]; then
#   echo "net.ipv4.tcp_mem setting failed."
#   exit -1
# fi

# ssh vagrant@${CLIENT_IP} "sudo sysctl -w net.ipv4.tcp_mem='$g1 $g2 $g3'"
# RES=`ssh vagrant@${CLIENT_IP} "sudo sysctl net.ipv4.tcp_mem | grep \"$g1[[:space:]]$g2[[:space:]]$g3\""`
# if [ -z "$RES" ]; then
#   echo "net.ipv4.tcp_mem setting failed."
#   exit -1
# fi


#ssh vagrant@$SERVER_IP  "sudo bpftool struct_ops register ./bpf_bbr.o"
#ssh vagrant@$SERVER_IP  "sudo sysctl -w net.ipv4.tcp_congestion_control=bpf_bbr"



#python3 idt_generator.py ${p} > idts.txt
#vagrant scp download_loop.sh client:~/
#vagrant scp download_loop.sh client1:~/

#vagrant scp iperf_client_loop.sh client:~/
#vagrant scp iperf_client_loop.sh client1:~/
#vagrant scp iperf_server_loop.sh server:~/


#vagrant scp log_queue.sh router1:~/

timeout 3000 /home/rluebben/.local/bin/sshlauncher -d run.sl
#rm idts.txt

rm -rf tmp_results
mkdir tmp_results
cd tmp_results

#collect results
declare -a arr=("server")
for s in "${arr[@]}"
do
  echo $s
  mkdir $s
  cd $s
  IP=`vagrant ssh $s -c "ip addr show eth0 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`
  #vagrant ssh ${s} -c "sudo cp /var/log/kern.log /home/vagrant; sudo chown vagrant /home/vagrant/kern.log"
  #vagrant scp ${s}:/home/vagrant/kern.log ./${s}_kern.log
  #vagrant scp ${s}:/home/vagrant/${s}.pcap ./
  #vagrant scp ${s}:/home/vagrant/itg_server.log ./
  scp vagrant@${IP}:/home/vagrant/server_trace.txt ./
  #vagrant ssh ${s} -c "sudo rm /home/vagrant/kern.log; sudo rm *.pcap; sudo rm itg_server.log; sudo rm /var/log/kern.log; sudo rm /home/vagrant/server_trace.txt"
  vagrant ssh ${s} -c "sudo rm /home/vagrant/server_trace.txt /home/vagrant/${s}.pcap"
  cd ..
done

declare -a arr=("client")
for s in "${arr[@]}"
do
  echo $s
  mkdir $s
  cd $s
  IP=`vagrant ssh $s -c "ip addr show eth0 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`
  #vagrant ssh ${s} -c "sudo cp /var/log/kern.log /home/vagrant; sudo chown vagrant /home/vagrant/kern.log"
  #vagrant scp ${s}:/home/vagrant/kern.log ./${s}_kern.log
  #vagrant scp ${s}:/home/vagrant/${s}.pcap ./
  #vagrant scp ${s}:/home/vagrant/itg_server.log ./
  scp vagrant@${IP}:/home/vagrant/result.jtl ./
  scp -r vagrant@${IP}:/home/vagrant/dashboard ./
  #vagrant ssh ${s} -c "sudo rm /home/vagrant/kern.log; sudo rm *.pcap; sudo rm itg_server.log; sudo rm /var/log/kern.log; sudo rm /home/vagrant/server_trace.txt"
  vagrant ssh ${s} -c "sudo rm /home/vagrant/result.jtl; sudo rm -rf /home/vagrant/dashboard  "
  cd ..
done

vagrant halt

cd ..
DIRN="results_cong_${CONG}_run_${r}_ctraffic_${CT}"
rm -rf ${DIRN}
mv tmp_results ${DIRN}

echo $DIRN
done
done
done

DIRN="rate_750_limit_600"
rm -rf ${DIRN}
mkdir ${DIRN}
mv results_cong_* ${DIRN}/
