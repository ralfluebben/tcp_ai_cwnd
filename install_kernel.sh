# Helper scripts to install packages, kernels, software, ssh pub keys


# Install kernel, ethtool and packages
#declare -a arr=("server" "client" "router1" "router2" "ct-server" "ct-client")
declare -a arr=("server")
num=$1
version="5.13.1+"
v_tag1="${version}_${version}-${num}"
v_tag2="${version}-$num"
echo $v_tag1
echo $v_tag2
for s in "${arr[@]}"
do
	echo $s;
    IP=`vagrant ssh $s -c "ip addr show eth0 | grep -Po 'inet \K[\d.]+'" | tr -d '[:space:]'`
    scp ../linux-headers-${v_tag1}_amd64.deb vagrant@${IP}:~/
    scp ../linux-image-${v_tag1}_amd64.deb vagrant@${IP}:~/
    scp ../linux-libc-dev_${v_tag2}_amd64.deb vagrant@${IP}:~/
    vagrant ssh ${s} -c "sudo dpkg -i linux-headers-${v_tag1}_amd64.deb"
    vagrant ssh ${s} -c "sudo dpkg -i linux-image-${v_tag1}_amd64.deb"
    vagrant ssh ${s} -c "sudo dpkg -i linux-libc-dev_${v_tag2}_amd64.deb"
    vagrant ssh ${s} -c "rm *.deb"
    vagrant reload ${s}
done
sleep 30
for s in "${arr[@]}"
do
	echo $s;
    kernel_version=`vagrant ssh ${s} -c "uname -a" | grep " #${v} "`
    if [ -z "$kernel_version" ]; then
        exit 1
    fi
done


exit


scp  ./linux-tools-5.13.0-14_5.13.0-14.14_amd64.deb vagrant@${IP}:~/
scp  ./linux-headers-5.13.0-14_5.13.0-14.14_all.deb vagrant@${IP}:~/
scp  ./linux-tools-5.13.0-14-generic_5.13.0-14.14_amd64.deb vagrant@${IP}:~/
scp  ./linux-libc-dev_5.13.0-14.14_amd64.deb vagrant@${IP}:~/
scp  ./linux-tools-common_5.13.0-14.14_all.deb vagrant@${IP}:~/ 
scp  ./linux-tools-host_5.13.0-14.14_all.deb vagrant@${IP}:~/
scp  ./linux-modules-extra-5.13.0-14-generic_5.13.0-14.14_amd64.deb vagrant@${IP}:~/
scp  ./linux-modules-5.13.0-14-generic_5.13.0-14.14_amd64.deb vagrant@${IP}:~/
scp  ./linux-image-unsigned-5.13.0-14-generic_5.13.0-14.14_amd64.deb vagrant@${IP}:~/
scp  ./linux-cloud-tools-5.13.0-14-generic_5.13.0-14.14_amd64.deb vagrant@${IP}:~/
scp  ./linux-cloud-tools-common_5.13.0-14.14_all.deb vagrant@${IP}:~/
scp  ./linux-headers-5.13.0-14-generic_5.13.0-14.14_amd64.deb vagrant@${IP}:~/
scp  ./linux-cloud-tools-5.13.0-14_5.13.0-14.14_amd64.deb vagrant@${IP}:~/
scp ./linux-buildinfo-5.13.0-14-generic_5.13.0-14.14_amd64.deb vagrant@${IP}:~/

scp bpf_bbr.o print_tcp_bpf vagrant@${IP}:~/


./linux-tools-5.13.0-14_5.13.0-14.14_amd64.deb
./linux-headers-5.13.0-14_5.13.0-14.14_all.deb
./linux-tools-5.13.0-14-generic_5.13.0-14.14_amd64.deb
./linux-libc-dev_5.13.0-14.14_amd64.deb
./linux-buildinfo-5.13.0-14-generic_5.13.0-14.14_amd64.deb
./linux-tools-common_5.13.0-14.14_all.deb
./linux-tools-host_5.13.0-14.14_all.deb
./linux-modules-extra-5.13.0-14-generic_5.13.0-14.14_amd64.deb
./linux-modules-5.13.0-14-generic_5.13.0-14.14_amd64.deb
./linux-image-unsigned-5.13.0-14-generic_5.13.0-14.14_amd64.deb
./linux-cloud-tools-5.13.0-14-generic_5.13.0-14.14_amd64.deb
./linux-cloud-tools-common_5.13.0-14.14_all.deb
./linux-headers-5.13.0-14-generic_5.13.0-14.14_amd64.deb
./linux-cloud-tools-5.13.0-14_5.13.0-14.14_amd64.deb




scp /home/rluebben/linux-headers-5.13.1_5.13.1-3_amd64.deb vagrant@${IP}:~/
scp /home/rluebben/linux-image-5.13.1_5.13.1-3_amd64.deb vagrant@${IP}:~/
scp /home/rluebben/linux-libc-dev_5.13.1-3_amd64.deb vagrant@${IP}:~/