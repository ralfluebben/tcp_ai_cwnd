[DEFAULT]
user: vagrant

[server_trace]
host: %(SL_SERVER_IP)s
command: sudo sh -c "echo 1 > /sys/kernel/debug/tracing/events/tcp/tcp_probe/enable"; sudo sh -c "echo \"sport == 80 || dport == 80\" > /sys/kernel/debug/tracing/events/tcp/tcp_probe/filter"; sudo sh -c "echo 1 > /sys/kernel/debug/tracing/tracing_on"; echo "tcp probe enabled"

[server_trace_pipe]
host: %(SL_SERVER_IP)s
command: sudo sh -c "cat /sys/kernel/debug/tracing/trace_pipe > server_trace.txt"
after: { 'server_trace': 'tcp probe enabled'}

[server_trace_mark]
host: %(SL_SERVER_IP)s
command: sudo sh -c "echo `date +%s.%N` > /sys/kernel/debug/tracing/trace_marker"
after: { 'server_trace': 'tcp probe enabled' }

[client_iperf]
host: %(SL_CLIENT_IP)s
command: sleep 1; echo "Server listening";echo "Server listening";echo "Server listening"; iperf -s -i 5

#[server_iperf]
#host: %(SL_SERVER_IP)s
#command: sudo iperf -c 192.168.3.13 -P 2 -t 5999 -Z reno -i 1
#after: { 'client_iperf': 'Server listening'}

[client_wget]
host: %(SL_CLIENT_IP)s
command: rm -rf dashboard result.jtl; sleep 10; timeout 200 ~/apache-jmeter-5.4.1/bin/jmeter -n -t ~/test.jmx -l result.jtl -e -o dashboard; echo "Download finished"
after: { 'server_trace': 'tcp probe enabled' }

[server_kill_tcpdump]
host: %(SL_CLIENT_IP)s
command: sudo killall iperf; 
after: {'client_wget':'Download finished' }

[client_kill_tcpdump]
host: %(SL_SERVER_IP)s
command: sudo killall iperf; sudo killall print_tcp_bpf
after: {'client_wget':'Download finished' }

[server_kill_trace_pipe]
host: %(SL_SERVER_IP)s
command: sudo pkill -f trace_pipe
after: {'client_wget':'Download finished' }