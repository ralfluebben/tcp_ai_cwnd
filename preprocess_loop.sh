#!/bin/bash
for r in `seq 1 20`
do
    DIRN="results_cong_reno_run_${r}_ctraffic_exp"
    cd ${DIRN}/server
    bash ~/git/tcp_ai_cwnd/preprocess_tcp_probe.sh
    cd ../..
done