# #!/bin/bash
# echo $1
# ID=$(echo $1 | cut -f 1 -d ' ')
# SRC_IP=$(echo $1 | cut -f 3 -d ' ')
# SRC_PORT=$(echo $1 | cut -f 4 -d ' ')
# DST_IP=$(echo $1 | cut -f 5 -d ' ')
# DST_PORT=$(echo $1 | cut -f 6 -d ' ')
# echo $ID
# echo $SRC_IP
# echo $SRC_PORT
# echo $DST_IP
# echo $DST_PORT

# file=server.pcap
# fileout=$(echo ${SRC_IP}__${SRC_PORT}__${DST_IP}__${DST_PORT} | tr -d '\r'  )
# echo $fileout
# #echo "tshark -r $file -2R \"tcp.stream == ${ID}\" -w \"$fileout.pcap\""
# #tshark -r $file -2R "tcp.stream == ${ID}" -w "$fileout.pcap"
# filter=$(echo src=${SRC_IP}:${SRC_PORT} dest=${DST_IP}:${DST_PORT} | tr -d '\r'  )
# echo $filter
# echo "time snd_nxt snd_una snd_cwnd ssthresh snd_wnd srtt rcv_wnd"  > $fileout.trace
# grep " $filter " server_trace.txt | awk '/tcp_probe:/{split($0,a,"tcp_probe:"); n=split(a[1],b," "); gsub(":","",b[n]); gsub(/^[ \t]+/,"",b[n]); gsub("="," ",a[2]); split(a[2],c," "); printf "%s", b[n]; for (i in c) { printf " %s",c[i]}; print"" }'  | cut -f 1,11,13,15,17,19,21,23 -d ' ' >> $fileout.trace

# echo "time snd_nxt snd_una snd_cwnd ssthresh snd_wnd srtt rcv_wnd min_rtt cookie rtt_var delivery_rate"  > tcp.trace
# zcat server_trace.txt.gz | awk '/tcp_probe:/{split($0,a,"tcp_probe:"); n=split(a[1],b," "); gsub(":","",b[n]); gsub(/^[ \t]+/,"",b[n]); gsub("="," ",a[2]); split(a[2],c," "); printf "%s", b[n]; for (i in c) { printf " %s",c[i]}; print"" }'  | cut -f 1,7,8,9,10,11,12,13,14,15,16,17 -d ' ' >> tcp.trace

# echo "ts saddr sport daddr dport sock_cookie tcpi_state tcpi_pacing_rate tcpi_max_pacing_rate tcpi_snd_cwnd tcpi_min_rtt tcpi_ca_state tcpi_retransmits tcpi_probes tcpi_backoff tcpi_rto tcpi_ato tcpi_last_data_sent tcpi_last_data_recv tcpi_last_ack_recv tcpi_pmtu tcpi_rcv_ssthresh tcpi_rtt tcpi_rttvar tcpi_snd_ssthresh tcpi_advmss tcpi_rcv_rtt tcpi_rcv_space tcpi_total_retrans tcpi_bytes_acked tcpi_bytes_received tcpi_notsent_bytes tcpi_segs_out tcpi_segs_in tcpi_busy_time tcpi_rwnd_limited tcpi_sndbuf_limited tcpi_data_segs_in tcpi_data_segs_out tcpi_delivery_rate_app_limited tcpi_delivery_rate tcpi_delivered tcpi_delivered_ce tcpi_bytes_sent tcpi_bytes_retrans tcpi_dsack_dups tcpi_reord_seen tcpi_rcv_ooopack tcpi_fastopen_client_fail bbr_bw bbr_minrtt bbr_pacing_gain bbr_cwnd_gain" > tcp.trace


split_trace () {
  echo "ts saddr sport daddr dport sock_cookie tcpi_state tcpi_pacing_rate tcpi_max_pacing_rate tcpi_snd_cwnd tcpi_min_rtt tcpi_ca_state tcpi_retransmits tcpi_probes tcpi_backoff tcpi_rto tcpi_ato tcpi_last_data_sent tcpi_last_data_recv tcpi_last_ack_recv tcpi_pmtu tcpi_rcv_ssthresh tcpi_rtt tcpi_rttvar tcpi_snd_ssthresh tcpi_advmss tcpi_rcv_rtt tcpi_rcv_space tcpi_total_retrans tcpi_bytes_acked tcpi_bytes_received tcpi_notsent_bytes tcpi_segs_out tcpi_segs_in tcpi_busy_time tcpi_rwnd_limited tcpi_sndbuf_limited tcpi_data_segs_in tcpi_data_segs_out tcpi_delivery_rate_app_limited tcpi_delivery_rate tcpi_delivered tcpi_delivered_ce tcpi_bytes_sent tcpi_bytes_retrans tcpi_dsack_dups tcpi_reord_seen tcpi_rcv_ooopack tcpi_fastopen_client_fail bbr_bw bbr_minrtt bbr_pacing_gain bbr_cwnd_gain" > tcp_${i}.trace

  cat results_cong_bbr_run_${i}_ctraffic_exp/server/server_trace.txt | awk '/tcp_probe:/{split($0,a,"tcp_probe:"); n=split(a[1],b," "); gsub(":","",b[n]); gsub(/^[ \t]+/,"",b[n]); gsub("="," ",a[2]); split(a[2],c," "); printf "%s", b[n]; for (i in c) { printf " %s",c[i]}; print"" }' >> tcp_${i}.trace
}

for i in `seq 1 30`
do
    split_trace &
done