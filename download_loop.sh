#!/bin/bash
download () {
    while true
    do
        wget -O /dev/null -o /dev/null http://192.168.1.10/file${1}.img
    done
}
for (( c=0; c<100; c++ ))
do
    download 256 &
    download 512 & 
    download 1024 &
done
trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT
wait