
import numpy as np
import pandas as pd
import os



h = lambda x: (int(x, 0))
h1 = lambda x: (int(x, 16))
h2 = lambda x: (int(float(x)*100000))

input_width = 100

from multiprocessing import Pool
fpath="tcp.trace"
data1=pd.read_csv(fpath, sep=' ', converters = {'time': h2, 'snd_nxt' : h, 'snd_una' : h, 'cookie': h1 },usecols=['time', 'snd_nxt', 'snd_una', 'snd_cwnd', 'ssthresh', 'srtt', 'cookie', 'min_rtt',  'rtt_var', 'delivery_rate'])
npdata=data1.to_numpy()
data1=None
n = np.unique(npdata[:,6])
def f(c):
  data = npdata[npdata[:,6]==c,:]
  if len(data) >= input_width and (data[-1,4] < 2147483647):
    idx = np.where(data[:,4] < 2147483647)[0][0]
    print(idx)
    first_ssthresh = data[idx,4]
    max_cwnd = np.max(data[0:idx,3])
    data[:,4] = 0
    data[10:,3] = 0
    data[(input_width-1),4] = max_cwnd
    print(data)
    return data[0:input_width,:]
  else:
    print( len(data), data[-1,4])
    return []

with Pool(96) as p:
  res=p.map(f, n)

r1=[]
short = 0
nothresh = 0
for r in res:
  if len(r) == input_width and (r[-1,4] < 2147483647):
    r1.append(r)
    print("o")
  elif len(r) < input_width:
    print("s")
    short+=1
  elif r[-1,4] >= 2147483647:
    nothresh+=1
    print("n")

print (short,nothresh,len(res) )

data=np.stack(r1, axis=0 )
np.save('data_input_width_'+ str(input_width) +'.npy', data)
