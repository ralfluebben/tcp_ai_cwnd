import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

from matplotlib import rcParams

rcParams['font.family'] = 'serif'
rcParams['font.sans-serif'] = ['Times']
rcParams['lines.markersize'] = 4
rcParams['font.size'] = 13

import matplotlib.pyplot as plt


df1=pd.read_feather("limit_150/data_150.ftr")
df2=pd.read_feather("limit_600/data_600.ftr")
df3=pd.read_feather("limit_300/data_300.ftr")
#df4=pd.read_feather("rate_750_limit_600/data_600.ftr")

samples=1500

train_len = 300
feature_columns=df1.columns.tolist()
data=np.concatenate((df1[0:(samples*train_len)].to_numpy(),df2[0:(samples*train_len)].to_numpy(),df3[0:(samples*train_len)].to_numpy()),axis=0)

#data=df4.to_numpy()
del df1
del df2
del df3

i = feature_columns.index("bbr_bw")
data[:,i]=data[:,i]*8/1e6
i = feature_columns.index("tcpi_delivery_rate")
data[:,i]=data[:,i]*8/1e6

num_features = len(feature_columns)
num_measurement = int(data.shape[0]*data.shape[1]/(num_features*train_len))
data_shaped=np.reshape(data,(num_measurement,train_len,num_features))

indices = np.arange(data_shaped.shape[0])
np.random.shuffle(indices)
data_shaped=data_shaped[indices]

print("Plot data")
max_n=25
for f in feature_columns:
    i = feature_columns.index(f)
    plt.figure(figsize=(12, int(3*max_n)))
    for n in range(max_n):
        ax=plt.subplot(max_n, 1, n+1)
        plt.plot(data_shaped[n,:,i], label=f, marker='.', zorder=-10)
    plt.xlabel('packet [#]')
    plt.tight_layout()
    fn="feature_plots/"+f+".pdf"
    print(fn)
    plt.savefig(fn)
    plt.close()

quit()
plt.close()
r=np.sum(data_shaped[:,:,14],axis=1)*8/1e6/np.sum(data_shaped[:,:,0],axis=1)
sns.ecdfplot(r, label="bytes/time" )
sns.ecdfplot(data_shaped[:,-1,19], label="deliveryrate" )
plt.legend()
#plt.xlim(0,5)
plt.savefig("fighistr.png")


t=np.cumsum(data_shaped[4,:,0])
plt.close()
m=142
i = feature_columns.index("tcpi_delivery_rate")
x=t[0:9]
y=data_shaped[m,0:9,i]
plt.plot(x,y,'.', label="features")
x=t[10:209]
y=data_shaped[m,10:209,i]
plt.plot(x,y,'.', label="gap")
x=t[209:215]
y=data_shaped[m,209:215,i]
plt.plot(x,y,'.', label="output")
plt.xlabel("time [ms]")
plt.ylabel("rate [MBit/s]")
plt.legend()
plt.savefig("tcpi_delivery_rate.pdf")

i = feature_columns.index("tcpi_snd_cwnd")

plt.close()
x=t[0:9]
y=data_shaped[m,0:9,i]
plt.plot(x,y,'.', label="features")
x=t[10:209]
y=data_shaped[m,10:209,i]
plt.plot(x,y,'.', label="gap")
x=t[209:215]
y=data_shaped[m,209:215,i]
plt.plot(x,y,'.', label="output")
plt.xlabel("time [ms]")
plt.ylabel("CWND [packets]")
plt.legend()
plt.savefig("tcpi_snd_cwnd.pdf")

i = feature_columns.index("tcpi_delivery_rate")
x=data_shaped[0:100,:,i]
y=data_shaped[0:100,:,3]
np.cov(x.T,y.T)

from sklearn.preprocessing import MinMaxScaler
cov=[]
for f in feature_columns:
    scaler1 = MinMaxScaler()
    scaler2 = MinMaxScaler()
    i = feature_columns.index("tcpi_delivery_rate")
    i1 = feature_columns.index(f)
    X1 = data_shaped[:,99,i].reshape(-1, 1)
    X1 = scaler1.fit_transform(X1)
    X1_avg = np.mean(X1)
    Y1 = data_shaped[:,199,i1].reshape(-1, 1)
    Y1 = scaler2.fit_transform(Y1)
    Y1_avg = np.mean(Y1)
    r1=(X1-X1_avg)
    r2=(Y1-Y1_avg)
    cov.append((f, np.abs(np.mean(r1*r2))))

l=sorted(cov, key=lambda d: d[1]) 

for i in l:
    print(i[0],i[1])


s=[]
for i in range(len(r1)):
    s.append((r1[i]*r2[i]))

print(np.mean(s))