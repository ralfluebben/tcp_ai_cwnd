import numpy as np
import pandas as pd
import os
from multiprocessing import Pool
import sys
import gc
import feather

# load data

#feature_columns = ["ts","saddr","sport","daddr","dport","cookie","ti_tcpi_ca_state","ti_tcpi_pacing_rate","ti_tcpi_snd_cwnd","ti_tcpi_min_rtt","ti_tcpi_ca_state","ti_tcpi_retransmits","ti_tcpi_probes","ti_tcpi_backoff","ti_tcpi_rto","ti_tcpi_ato","ti_tcpi_last_data_sent","ti_tcpi_last_data_recv","ti_tcpi_last_ack_recv","ti_tcpi_pmtu","ti_tcpi_rcv_ssthresh","ti_tcpi_rtt","ti_tcpi_rttvar","ti_tcpi_snd_ssthresh","ti_tcpi_advmss","ti_tcpi_rcv_rtt","ti_tcpi_rcv_space","ti_tcpi_total_retrans","ti_tcpi_bytes_acked","ti_tcpi_bytes_received","ti_tcpi_notsent_bytes","ti_tcpi_segs_out","ti_tcpi_segs_in","ti_tcpi_busy_time","ti_tcpi_rwnd_limited","ti_tcpi_sndbuf_limited","ti_tcpi_data_segs_in","ti_tcpi_data_segs_out","ti_tcpi_delivery_rate_app_limited","ti_tcpi_delivery_rate","ti_tcpi_delivered","ti_tcpi_delivered_ce","ti_tcpi_bytes_sent","ti_tcpi_bytes_retrans","ti_tcpi_dsack_dups","ti_tcpi_reord_seen","ti_tcpi_rcv_ooopack","ti_tcpi_fastopen_client_fail","bbr_bw","bbr_bbr_min_rtt","bbr_bbr_pacing_gain","bbr_bbr_cwnd_gain"]

feature_columns = ["ts",
"saddr",
"sport",
"daddr",
"dport",
"sock_cookie",
"tcpi_state", 
"tcpi_pacing_rate", 
"tcpi_max_pacing_rate",
"tcpi_snd_cwnd", 
"tcpi_min_rtt", 
"tcpi_ca_state", 
"tcpi_retransmits", 
"tcpi_probes",
"tcpi_backoff",
"tcpi_rto",
"tcpi_ato",
"tcpi_last_data_sent",
"tcpi_last_data_recv",
"tcpi_last_ack_recv",
"tcpi_pmtu",
"tcpi_rcv_ssthresh",
"tcpi_rtt",
"tcpi_rttvar",
"tcpi_snd_ssthresh",
"tcpi_advmss",
"tcpi_rcv_rtt",
"tcpi_rcv_space",
"tcpi_total_retrans",
"tcpi_bytes_acked",
"tcpi_bytes_received",
"tcpi_notsent_bytes",
"tcpi_segs_out",
"tcpi_segs_in",
"tcpi_busy_time",
"tcpi_rwnd_limited",
"tcpi_sndbuf_limited",
"tcpi_data_segs_in",
"tcpi_data_segs_out",
"tcpi_delivery_rate_app_limited",
"tcpi_delivery_rate",
"tcpi_delivered",
"tcpi_delivered_ce",
"tcpi_bytes_sent",
"tcpi_bytes_retrans",
"tcpi_dsack_dups",
"tcpi_reord_seen",
"tcpi_rcv_ooopack",
"tcpi_fastopen_client_fail",
"bbr_bw",
"bbr_minrtt",
"bbr_pacing_gain",
"bbr_cwnd_gain", ]

features_to_inter = [ "tcpi_busy_time", 'tcpi_bytes_acked', 'tcpi_bytes_retrans','tcpi_bytes_sent','tcpi_data_segs_out', 'tcpi_delivered','tcpi_rwnd_limited', 'tcpi_segs_in', 'tcpi_segs_out', 'tcpi_total_retrans'  ]

features_to_remove_corr = [ "tcpi_segs_in",  "tcpi_data_segs_out", "tcpi_backoff", "tcpi_retransmits",  "tcpi_snd_ssthresh", "bbr_cwnd_gain", "bbr_pacing_gain"   ]

features_to_remove = [ 'saddr', 'daddr', 'dport', 'tcpi_ato', 'tcpi_bytes_received','tcpi_data_segs_in', 'sport', 'tcpi_state', 'tcpi_max_pacing_rate', 'tcpi_pmtu', 'tcpi_rcv_ssthresh', 'tcpi_advmss', 'tcpi_rcv_rtt', 'tcpi_rcv_space', 'tcpi_delivered_ce', 'tcpi_rcv_ooopack', 'tcpi_fastopen_client_fail']

to_del = []
for fea in features_to_remove:
  i = feature_columns.index(fea)
  to_del.append(i)

for fea in features_to_remove_corr:
  i = feature_columns.index(fea)
  to_del.append(i)

i_to_del = sorted(set(to_del),reverse=True)

for index in i_to_del:
  del feature_columns[index] 

to_del = []
for fea in features_to_remove:
  if fea in features_to_inter:
    i = features_to_inter.index(fea)
    to_del.append(i)

for fea in features_to_remove_corr:
  if fea in features_to_inter:
    i = features_to_inter.index(fea)
    to_del.append(i)

i_to_del = sorted(set(to_del),reverse=True)

for index in i_to_del:
  del features_to_inter[index] 


min_bytes_acked = 100000
min_bw = 1000
train_len = 300
debug = True

def f(c):
  fpath="tcp_" + str(c) + ".trace" 
  data1=pd.read_csv(fpath, sep=' ', usecols=feature_columns)
  print(data1.columns)
  # split by cookie
  d = None

  f = feature_columns.index("sock_cookie")
  n = np.unique(data1["sock_cookie"].to_numpy())
  if debug == True:
      print(f, n)

  i_ack = feature_columns.index("tcpi_bytes_acked")
  i_bw = feature_columns.index("bbr_bw")
  i_ts = feature_columns.index("ts")
  data_list = []
  for i in n:
    # filter on cookie
    print(i)
    data = data1[(data1["sock_cookie"]==i)].to_numpy()
    # shift ts to zero
    bw=np.nan_to_num(data[0:10,i_bw].astype(np.float))
    if (data[-1,i_ack] > min_bytes_acked) and (np.mean(bw) > 1000) and (data.shape[0] >= train_len):
      data[:,i_ts] = data[:,i_ts]-data[0,i_ts]
      data[1:train_len,i_ts] = data[1:train_len,i_ts]-data[0:(train_len-1),i_ts]
      for f in features_to_inter:
        f_i = feature_columns.index(f)
        data[1:train_len,f_i] = data[1:train_len,f_i]-data[0:(train_len-1),f_i]
      data_list.append(data[0:train_len,:])
    gc.collect()
  return np.concatenate((data_list),axis=0)


r = range(1,31)
with Pool(10) as p:
  res=p.map(f, r)
  d = res[0]
  res[0] = None 
  gc.collect()
  for r in range(1,len(res)):
      print("Concat:",r)
      d=np.concatenate((d,res[r]),axis=0)
      res[r] = None
      gc.collect()
  print(d.shape)
df = pd.DataFrame(data=d, columns=feature_columns)
df.to_feather("data.ftr")
#df.to_feather("data_300.ftr")
#df.to_feather("data_600.ftr")
