import numpy as np
import pandas as pd
import xarray as xr
df1=pd.read_feather("limit_150/data.ftr")
df2=pd.read_feather("limit_600/data.ftr")
df3=pd.read_feather("limit_300/data.ftr")

samples=150000

train_len = 300
feature_columns=df1.columns.tolist()
data=np.concatenate((df1[0:(samples*train_len)].to_numpy(),df2[0:(samples*train_len)].to_numpy(),df3[0:(samples*train_len)].to_numpy()),axis=0)

del df1
del df2
del df3

i = feature_columns.index("bbr_bw")
data[:,i]=data[:,i]*8/1e6
i = feature_columns.index("tcpi_delivery_rate")
data[:,i]=data[:,i]*8/1e6

num_features = len(feature_columns)
num_measurement = int(data.shape[0]*data.shape[1]/(num_features*train_len))
data_shaped=np.reshape(data,(num_measurement,train_len,num_features))


data_old = data_shaped
print("Shuffle data")
#np.random.seed(42)
indices = np.arange(data_shaped.shape[0])
np.random.shuffle(indices)
data_shaped=data_shaped[indices]

if np.array_equal(data_shaped[0,:,:],data_old[indices[0],:,:]) == False:
  raise Exception("invalid shuffle")

del data_old

print(data_shaped.shape)
df=xr.DataArray(data_shaped, dims=["measurement","packetnum","values"])
df["measurement"]=list(range(0,data_shaped.shape[0]))
df["packetnum"]=list(range(0,data_shaped.shape[1]))
df["values"]=feature_columns

df.to_netcdf("data.nc")
df.close()